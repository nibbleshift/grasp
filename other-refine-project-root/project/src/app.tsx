// Code generated by EntKit. DO NOT EDIT.
// ---------------------------------------------------------
//
// Copyright (C) 2023 EntKit. All Rights Reserved.
//
// This code is part of the EntKit library and is generated
// automatically to ensure optimal functionality and maintainability.
// Any changes made directly to this file may be overwritten
// by future code generation, leading to unexpected behavior.
//
// Please refer to the EntKit documentation for instructions on
// how to modify the library, extend its functionality or contribute
// to the project: https://entkit.com
// ---------------------------------------------------------
import React, { useState } from "react";
import { AuthBindings, Authenticated, Refine } from "@refinedev/core";
import {
    ErrorComponent,
    notificationProvider,
    RefineThemes,
} from "@refinedev/antd";
import { ConfigProvider } from "antd";
import "@refinedev/antd/dist/reset.css";
import { GraphQLClient } from "graphql-request";
import dataProvider from "./data-provider";
import routerProvider, {
    NavigateToResource,
    CatchAllNavigate,
    UnsavedChangesNotifier,
} from "@refinedev/react-router-v6";
import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
import { RoutesBundle } from "./routes";
import * as AntdIcons from "@ant-design/icons";

function App() {
    const client = new GraphQLClient(window.environment.graphqlUrl);

    return (
        <BrowserRouter>
            <ConfigProvider theme={RefineThemes.Blue}>
                <Refine
                    routerProvider={routerProvider}
                    dataProvider={dataProvider(client)}
                    notificationProvider={notificationProvider}
                    catchAll={<ErrorComponent />}
                    resources={[
                        {
                            name: "document",
                            list: window.environment.appPath + "document",
                            show:
                                window.environment.appPath +
                                "document/show/:id",
                            create:
                                window.environment.appPath + "document/create",
                            edit:
                                window.environment.appPath +
                                "document/edit/:id",
                            meta: {
                                icon: <AntdIcons.FileOutlined />,
                            },
                        },
                        {
                            name: "source",
                            list: window.environment.appPath + "source",
                            show:
                                window.environment.appPath + "source/show/:id",
                            create:
                                window.environment.appPath + "source/create",
                            edit:
                                window.environment.appPath + "source/edit/:id",
                            meta: {
                                icon: <AntdIcons.LinkOutlined />,
                            },
                        },
                    ]}
                >
                    <RoutesBundle />
                    <UnsavedChangesNotifier />
                </Refine>
            </ConfigProvider>
        </BrowserRouter>
    );
}

export default App;
