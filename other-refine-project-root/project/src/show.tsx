// Code generated by EntKit. DO NOT EDIT.
// ---------------------------------------------------------
//
// Copyright (C) 2023 EntKit. All Rights Reserved.
//
// This code is part of the EntKit library and is generated
// automatically to ensure optimal functionality and maintainability.
// Any changes made directly to this file may be overwritten
// by future code generation, leading to unexpected behavior.
//
// Please refer to the EntKit documentation for instructions on
// how to modify the library, extend its functionality or contribute
// to the project: https://entkit.com
// ---------------------------------------------------------
import React, { useState } from "react";
import { useShow } from "@refinedev/core";
import * as RA from "@refinedev/antd";
import * as Antd from "antd";
import * as AntdIcons from "@ant-design/icons";

import * as Lists from "./list";
import * as Diagram from "./diagram";
import * as Interfaces from "./typedefs";
import * as View from "./view";
import * as Custom from "./custom";
import * as Action from "./action";

export type DocumentShowProps = {
    id?: Interfaces.DemoID;
    withEdges?: boolean;
} & RA.ShowProps;
export const DocumentShow: React.FC<DocumentShowProps> = ({
    id,
    withEdges,
    ...showProps
}) => {
    const { queryResult } = useShow<Interfaces.DemoDocumentInterface>({
        resource: "document",
        id,
        metaData: {
            fields: ["id", "name"],
        },
    });

    const { data, isLoading } = queryResult;
    const record = data?.data;

    if (!record) {
        return <></>;
    }

    return (
        <RA.Show
            isLoading={isLoading}
            headerButtons={() => (
                <>
                    <Action.DocumentListAction recordItemIDs={[record.id]} />
                    <Action.DocumentEditAction recordItemIDs={[record.id]} />
                    <Action.DocumentDeleteAction recordItemIDs={[record.id]} />
                </>
            )}
            {...showProps}
        >
            <Antd.Typography.Title level={5}>Id</Antd.Typography.Title>
            <View.DemoUUIDViewOnShow value={record?.id} />
            <Antd.Typography.Title level={5}>Name</Antd.Typography.Title>
            <View.DemoStringViewOnShow value={record?.name} />

            {withEdges ? (
                <>
                    <Antd.Typography.Title level={3}>
                        Edges
                    </Antd.Typography.Title>
                    <Antd.Descriptions></Antd.Descriptions>
                    <Antd.Tabs defaultActiveKey="0" items={[]} />
                </>
            ) : null}
        </RA.Show>
    );
};

export const DocumentMainShow: React.FC = () => {
    return <DocumentShow withEdges={true} />;
};

export const DocumentPartialShow: React.FC = () => {
    return <DocumentShow withEdges={false} />;
};

export type SourceShowProps = {
    id?: Interfaces.DemoID;
    withEdges?: boolean;
} & RA.ShowProps;
export const SourceShow: React.FC<SourceShowProps> = ({
    id,
    withEdges,
    ...showProps
}) => {
    const { queryResult } = useShow<Interfaces.DemoSourceInterface>({
        resource: "source",
        id,
        metaData: {
            fields: ["id", "name"],
        },
    });

    const { data, isLoading } = queryResult;
    const record = data?.data;

    if (!record) {
        return <></>;
    }

    return (
        <RA.Show
            isLoading={isLoading}
            headerButtons={() => (
                <>
                    <Action.SourceListAction recordItemIDs={[record.id]} />
                    <Action.SourceEditAction recordItemIDs={[record.id]} />
                    <Action.SourceDeleteAction recordItemIDs={[record.id]} />
                </>
            )}
            {...showProps}
        >
            <Antd.Typography.Title level={5}>Id</Antd.Typography.Title>
            <View.DemoUUIDViewOnShow value={record?.id} />
            <Antd.Typography.Title level={5}>Name</Antd.Typography.Title>
            <View.DemoStringViewOnShow value={record?.name} />

            {withEdges ? (
                <>
                    <Antd.Typography.Title level={3}>
                        Edges
                    </Antd.Typography.Title>
                    <Antd.Descriptions></Antd.Descriptions>
                    <Antd.Tabs defaultActiveKey="0" items={[]} />
                </>
            ) : null}
        </RA.Show>
    );
};

export const SourceMainShow: React.FC = () => {
    return <SourceShow withEdges={true} />;
};

export const SourcePartialShow: React.FC = () => {
    return <SourceShow withEdges={false} />;
};
