// Code generated by EntKit. DO NOT EDIT.
// ---------------------------------------------------------
//
// Copyright (C) 2023 EntKit. All Rights Reserved.
//
// This code is part of the EntKit library and is generated
// automatically to ensure optimal functionality and maintainability.
// Any changes made directly to this file may be overwritten
// by future code generation, leading to unexpected behavior.
//
// Please refer to the EntKit documentation for instructions on
// how to modify the library, extend its functionality or contribute
// to the project: https://entkit.com
// ---------------------------------------------------------

import React, { useState } from "react";
import * as RA from "@refinedev/antd";
import * as Tables from "./tables";
import * as Type from "./typedefs";
import * as Action from "./action";
export type DocumentListProps = RA.ListProps & {
    tableProps?: Tables.DocumentTableProps;
};
export const DocumentList: React.FC<DocumentListProps> = ({
    tableProps,
    ...props
}) => {
    const can = true;

    const [selectedRowKeys, setSelectedRowKeys] = useState<Type.DemoID[]>([]);
    const rowSelection = { selectedRowKeys, onChange: setSelectedRowKeys };

    return can ? (
        <RA.List
            {...props}
            resource="document"
            headerButtons={({ defaultButtons }) => (
                <>
                    <Action.DocumentListAction
                        key="DocumentListAction"
                        recordItemIDs={selectedRowKeys}
                    />
                    <Action.DocumentCreateAction
                        key="DocumentCreateAction"
                        recordItemIDs={selectedRowKeys}
                    />

                    {selectedRowKeys.length ? (
                        <>
                            <Action.DocumentDeleteAction
                                key="DocumentDeleteAction"
                                recordItemIDs={selectedRowKeys}
                                onSuccess={() => setSelectedRowKeys([])}
                            />
                        </>
                    ) : null}
                </>
            )}
        >
            <Tables.DocumentTable {...tableProps} rowSelection={rowSelection} />
        </RA.List>
    ) : null;
};
export type SourceListProps = RA.ListProps & {
    tableProps?: Tables.SourceTableProps;
};
export const SourceList: React.FC<SourceListProps> = ({
    tableProps,
    ...props
}) => {
    const can = true;

    const [selectedRowKeys, setSelectedRowKeys] = useState<Type.DemoID[]>([]);
    const rowSelection = { selectedRowKeys, onChange: setSelectedRowKeys };

    return can ? (
        <RA.List
            {...props}
            resource="source"
            headerButtons={({ defaultButtons }) => (
                <>
                    <Action.SourceListAction
                        key="SourceListAction"
                        recordItemIDs={selectedRowKeys}
                    />
                    <Action.SourceCreateAction
                        key="SourceCreateAction"
                        recordItemIDs={selectedRowKeys}
                    />

                    {selectedRowKeys.length ? (
                        <>
                            <Action.SourceDeleteAction
                                key="SourceDeleteAction"
                                recordItemIDs={selectedRowKeys}
                                onSuccess={() => setSelectedRowKeys([])}
                            />
                        </>
                    ) : null}
                </>
            )}
        >
            <Tables.SourceTable {...tableProps} rowSelection={rowSelection} />
        </RA.List>
    ) : null;
};
