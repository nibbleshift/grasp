// Code generated by EntKit. DO NOT EDIT.
// ---------------------------------------------------------
//
// Copyright (C) 2023 EntKit. All Rights Reserved.
//
// This code is part of the EntKit library and is generated
// automatically to ensure optimal functionality and maintainability.
// Any changes made directly to this file may be overwritten
// by future code generation, leading to unexpected behavior.
//
// Please refer to the EntKit documentation for instructions on
// how to modify the library, extend its functionality or contribute
// to the project: https://entkit.com
// ---------------------------------------------------------

import React, { useState } from "react";
import { HttpError } from "@refinedev/core";
import * as RA from "@refinedev/antd";
import * as Antd from "antd";
import * as AntdIcons from "@ant-design/icons";
import * as Interfaces from "./typedefs";
import { Cursors } from "./data-provider";
import * as Custom from "./custom";
import * as View from "./view";
import * as Action from "./action";

export type DocumentTableProps =
    Antd.TableProps<Interfaces.DemoDocumentInterface> & {
        extendTable?: RA.useTableProps<
            Interfaces.DemoDocumentInterface,
            HttpError,
            any,
            Interfaces.DemoDocumentInterface
        >;
    };
export const DocumentTable: React.FC<DocumentTableProps> = ({
    extendTable,
    ...props
}) => {
    const [cursors, setCursors] = useState<Cursors>({ first: 10 });
    const [perPage, setPerPage] = useState<number>(10);
    const table = RA.useTable<Interfaces.DemoDocumentInterface>({
        resource: "document",
        initialSorter: [
            {
                field: "id",
                order: "asc",
            },
        ],
        initialFilter: [
            {
                field: "id",
                value: null,
                operator: "eq",
            },
            {
                field: "name",
                value: null,
                operator: "contains",
            },
        ],
        metaData: {
            fields: ["id", "name"],
            cursors,
        },
        hasPagination: true,
        ...extendTable,
    });

    const data = table.tableQueryResult.data as any;

    return (
        <>
            <Antd.Table
                {...table.tableProps}
                pagination={false}
                rowKey="id"
                {...props}
            >
                {/* region Fields */}
                <Antd.Table.Column
                    dataIndex="id"
                    title="Id"
                    sorter={{}}
                    render={(value) => {
                        return <View.DemoUUIDViewOnList value={value} />;
                    }}
                    filterDropdown={(props) => (
                        <RA.FilterDropdown {...props}>
                            <Antd.Input />
                        </RA.FilterDropdown>
                    )}
                    defaultSortOrder={RA.getDefaultSortOrder(
                        "id",
                        table.sorter,
                    )}
                />
                <Antd.Table.Column
                    dataIndex="name"
                    title="Name"
                    sorter={{}}
                    render={(value) => {
                        return <View.DemoStringViewOnList value={value} />;
                    }}
                    filterDropdown={(props) => (
                        <RA.FilterDropdown {...props}>
                            <Antd.Input />
                        </RA.FilterDropdown>
                    )}
                    defaultSortOrder={RA.getDefaultSortOrder(
                        "name",
                        table.sorter,
                    )}
                />
                {/* endregion */}

                {/* region Edges */}
                {/* endregion Edges*/}

                <Antd.Table.Column<Interfaces.DemoDocumentInterface>
                    title="Actions"
                    dataIndex="actions"
                    render={(_, record) => (
                        <Antd.Space>
                            <Action.DocumentShowAction
                                recordItemIDs={[record.id]}
                                size="small"
                                hideText={true}
                            />
                            <Action.DocumentEditAction
                                recordItemIDs={[record.id]}
                                size="small"
                                hideText={true}
                            />
                            <Action.DocumentDeleteAction
                                recordItemIDs={[record.id]}
                                size="small"
                                hideText={true}
                            />
                        </Antd.Space>
                    )}
                />
            </Antd.Table>

            <Antd.Space style={{ marginTop: 20 }}>
                <Antd.Typography.Text type="secondary">
                    Total {data?.total || 0}
                </Antd.Typography.Text>
                <Antd.Button
                    disabled={!Boolean(data?.pageInfo?.hasPreviousPage)}
                    onClick={() => {
                        setCursors((ov) => ({
                            ...ov,
                            before: data?.pageInfo?.startCursor,
                            last: perPage,
                            after: undefined,
                            first: undefined,
                        }));
                    }}
                >
                    <AntdIcons.LeftOutlined />
                    Prev
                </Antd.Button>
                <Antd.Button
                    disabled={!Boolean(data?.pageInfo?.hasNextPage)}
                    onClick={() => {
                        setCursors((ov) => {
                            return {
                                ...ov,
                                after: data?.pageInfo?.endCursor,
                                first: perPage,
                                before: undefined,
                                last: undefined,
                            };
                        });
                    }}
                >
                    Next
                    <AntdIcons.RightOutlined />
                </Antd.Button>
                <Antd.Select
                    labelInValue
                    defaultValue={{ value: 10, label: "10 / page" }}
                    style={{ width: 110 }}
                    onChange={(value) => {
                        setPerPage(value.value);
                        setCursors((ov) => ({
                            ...ov,
                            // Return to first page
                            first: value.value,
                            last: undefined,
                            before: undefined,
                            after: undefined,
                        }));
                    }}
                    options={[
                        { value: 10, label: "10 / page" },
                        { value: 20, label: "20 / page" },
                        { value: 50, label: "50 / page" },
                        { value: 100, label: "100 / page" },
                    ]}
                />
            </Antd.Space>
        </>
    );
};
export type SourceTableProps =
    Antd.TableProps<Interfaces.DemoSourceInterface> & {
        extendTable?: RA.useTableProps<
            Interfaces.DemoSourceInterface,
            HttpError,
            any,
            Interfaces.DemoSourceInterface
        >;
    };
export const SourceTable: React.FC<SourceTableProps> = ({
    extendTable,
    ...props
}) => {
    const [cursors, setCursors] = useState<Cursors>({ first: 10 });
    const [perPage, setPerPage] = useState<number>(10);
    const table = RA.useTable<Interfaces.DemoSourceInterface>({
        resource: "source",
        initialSorter: [
            {
                field: "id",
                order: "asc",
            },
        ],
        initialFilter: [
            {
                field: "id",
                value: null,
                operator: "eq",
            },
            {
                field: "name",
                value: null,
                operator: "contains",
            },
        ],
        metaData: {
            fields: ["id", "name"],
            cursors,
        },
        hasPagination: true,
        ...extendTable,
    });

    const data = table.tableQueryResult.data as any;

    return (
        <>
            <Antd.Table
                {...table.tableProps}
                pagination={false}
                rowKey="id"
                {...props}
            >
                {/* region Fields */}
                <Antd.Table.Column
                    dataIndex="id"
                    title="Id"
                    sorter={{}}
                    render={(value) => {
                        return <View.DemoUUIDViewOnList value={value} />;
                    }}
                    filterDropdown={(props) => (
                        <RA.FilterDropdown {...props}>
                            <Antd.Input />
                        </RA.FilterDropdown>
                    )}
                    defaultSortOrder={RA.getDefaultSortOrder(
                        "id",
                        table.sorter,
                    )}
                />
                <Antd.Table.Column
                    dataIndex="name"
                    title="Name"
                    sorter={{}}
                    render={(value) => {
                        return <View.DemoStringViewOnList value={value} />;
                    }}
                    filterDropdown={(props) => (
                        <RA.FilterDropdown {...props}>
                            <Antd.Input />
                        </RA.FilterDropdown>
                    )}
                    defaultSortOrder={RA.getDefaultSortOrder(
                        "name",
                        table.sorter,
                    )}
                />
                {/* endregion */}

                {/* region Edges */}
                {/* endregion Edges*/}

                <Antd.Table.Column<Interfaces.DemoSourceInterface>
                    title="Actions"
                    dataIndex="actions"
                    render={(_, record) => (
                        <Antd.Space>
                            <Action.SourceShowAction
                                recordItemIDs={[record.id]}
                                size="small"
                                hideText={true}
                            />
                            <Action.SourceEditAction
                                recordItemIDs={[record.id]}
                                size="small"
                                hideText={true}
                            />
                            <Action.SourceDeleteAction
                                recordItemIDs={[record.id]}
                                size="small"
                                hideText={true}
                            />
                        </Antd.Space>
                    )}
                />
            </Antd.Table>

            <Antd.Space style={{ marginTop: 20 }}>
                <Antd.Typography.Text type="secondary">
                    Total {data?.total || 0}
                </Antd.Typography.Text>
                <Antd.Button
                    disabled={!Boolean(data?.pageInfo?.hasPreviousPage)}
                    onClick={() => {
                        setCursors((ov) => ({
                            ...ov,
                            before: data?.pageInfo?.startCursor,
                            last: perPage,
                            after: undefined,
                            first: undefined,
                        }));
                    }}
                >
                    <AntdIcons.LeftOutlined />
                    Prev
                </Antd.Button>
                <Antd.Button
                    disabled={!Boolean(data?.pageInfo?.hasNextPage)}
                    onClick={() => {
                        setCursors((ov) => {
                            return {
                                ...ov,
                                after: data?.pageInfo?.endCursor,
                                first: perPage,
                                before: undefined,
                                last: undefined,
                            };
                        });
                    }}
                >
                    Next
                    <AntdIcons.RightOutlined />
                </Antd.Button>
                <Antd.Select
                    labelInValue
                    defaultValue={{ value: 10, label: "10 / page" }}
                    style={{ width: 110 }}
                    onChange={(value) => {
                        setPerPage(value.value);
                        setCursors((ov) => ({
                            ...ov,
                            // Return to first page
                            first: value.value,
                            last: undefined,
                            before: undefined,
                            after: undefined,
                        }));
                    }}
                    options={[
                        { value: 10, label: "10 / page" },
                        { value: 20, label: "20 / page" },
                        { value: 50, label: "50 / page" },
                        { value: 100, label: "100 / page" },
                    ]}
                />
            </Antd.Space>
        </>
    );
};
