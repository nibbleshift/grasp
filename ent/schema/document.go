package schema

import (
	"entgo.io/contrib/entgql"
	"entgo.io/ent"
	"entgo.io/ent/entc/gen"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/field"
	"github.com/entkit/entkit/v2"
	"github.com/google/uuid"
)

// Document holds the schema definition for the Document entity.
type Document struct {
	ent.Schema
}

// Fields of the Document.
func (Document) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).
			Default(uuid.New).
			Annotations(
				entgql.OrderField("ID"),
			),
		field.String("name").
			Annotations(
				entkit.FilterOperator(gen.Contains),
				entgql.OrderField("NAME"),
			),
	}
}

// Edges of the Document.
func (Document) Edges() []ent.Edge {
	return nil
}

func (Document) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entgql.RelayConnection(),
		entgql.QueryField(),
		entkit.Icon("FileOutlined"),
		entkit.IndexRoute(),
		entgql.Mutations(entgql.MutationCreate(), entgql.MutationUpdate()),
	}
}
