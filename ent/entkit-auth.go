// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"fmt"
	"net/http"

	"github.com/entkit/entkit/v2"
	"github.com/gin-gonic/gin"
)

// _entkitDefaultValue to provide default values from entc definition and also get ability to provide from caller
func _entkitDefaultValue(value string, defaultValue string) string {
	if value == "" {
		return defaultValue
	}
	return value
}

func DemoAuthMiddleware(
	next http.Handler,
) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Entkit: Authentication/Authorization is currently disabled. To utilize DemoAuthMiddleware, please enable this feature.")
		next.ServeHTTP(w, r)
	})
}

func DemoAuthGinMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("Entkit: Authentication/Authorization is currently disabled. To utilize DemoAuthGinMiddleware, please enable this feature.")
		c.Next()
	}
}

type DemoResource int

const (
	DemoDocumentResource DemoResource = iota
	DemoSourceResource
)

func (e DemoResource) String() string {
	switch e {
	case DemoDocumentResource:
		return "DemoDocument"
	case DemoSourceResource:
		return "DemoSource"
	default:
		return "unknown"
	}
}

type DemoScope int

const (
	DemoReadScope DemoScope = iota
	DemoCreateScope
	DemoUpdateScope
	DemoDeleteScope
)

func (e DemoScope) String() string {
	switch e {
	case DemoReadScope:
		return "DemoRead"
	case DemoCreateScope:
		return "DemoCreate"
	case DemoUpdateScope:
		return "DemoUpdate"
	case DemoDeleteScope:
		return "DemoDelete"
	default:
		return "unknown"
	}
}

func DemoAuthorizeByResource(ctx context.Context, resource DemoResource, scope DemoScope) error {
	fmt.Println("Entkit: Authentication/Authorization is currently disabled. To utilize DemoAuthorizeByResource, please enable this feature.")
	return nil
}

func DemoEnforceDocumentRead(ctx context.Context) error {
	return DemoAuthorizeByResource(ctx, DemoDocumentResource, DemoReadScope)
}

func DemoEnforceDocumentCreate(ctx context.Context) error {
	return DemoAuthorizeByResource(ctx, DemoDocumentResource, DemoCreateScope)
}

func DemoEnforceDocumentUpdate(ctx context.Context) error {
	return DemoAuthorizeByResource(ctx, DemoDocumentResource, DemoUpdateScope)
}

func DemoEnforceDocumentDelete(ctx context.Context) error {
	return DemoAuthorizeByResource(ctx, DemoDocumentResource, DemoDeleteScope)
}

func DemoEnforceSourceRead(ctx context.Context) error {
	return DemoAuthorizeByResource(ctx, DemoSourceResource, DemoReadScope)
}

func DemoEnforceSourceCreate(ctx context.Context) error {
	return DemoAuthorizeByResource(ctx, DemoSourceResource, DemoCreateScope)
}

func DemoEnforceSourceUpdate(ctx context.Context) error {
	return DemoAuthorizeByResource(ctx, DemoSourceResource, DemoUpdateScope)
}

func DemoEnforceSourceDelete(ctx context.Context) error {
	return DemoAuthorizeByResource(ctx, DemoSourceResource, DemoDeleteScope)
}

func DemoAuthContextFromRequestContext(ctx context.Context) (*entkit.AuthContext, error) {
	fmt.Println("Entkit: Authentication/Authorization is currently disabled. To utilize DemoAuthContextFromRequestContext, please enable this feature.")
	return nil, nil
}
