// Code generated by ent, DO NOT EDIT.

package ent

// CreateDocumentInput represents a mutation input for creating documents.
type CreateDocumentInput struct {
	Name string
}

// Mutate applies the CreateDocumentInput on the DocumentMutation builder.
func (i *CreateDocumentInput) Mutate(m *DocumentMutation) {
	m.SetName(i.Name)
}

// SetInput applies the change-set in the CreateDocumentInput on the DocumentCreate builder.
func (c *DocumentCreate) SetInput(i CreateDocumentInput) *DocumentCreate {
	i.Mutate(c.Mutation())
	return c
}

// UpdateDocumentInput represents a mutation input for updating documents.
type UpdateDocumentInput struct {
	Name *string
}

// Mutate applies the UpdateDocumentInput on the DocumentMutation builder.
func (i *UpdateDocumentInput) Mutate(m *DocumentMutation) {
	if v := i.Name; v != nil {
		m.SetName(*v)
	}
}

// SetInput applies the change-set in the UpdateDocumentInput on the DocumentUpdate builder.
func (c *DocumentUpdate) SetInput(i UpdateDocumentInput) *DocumentUpdate {
	i.Mutate(c.Mutation())
	return c
}

// SetInput applies the change-set in the UpdateDocumentInput on the DocumentUpdateOne builder.
func (c *DocumentUpdateOne) SetInput(i UpdateDocumentInput) *DocumentUpdateOne {
	i.Mutate(c.Mutation())
	return c
}

// CreateSourceInput represents a mutation input for creating sources.
type CreateSourceInput struct {
	Name string
}

// Mutate applies the CreateSourceInput on the SourceMutation builder.
func (i *CreateSourceInput) Mutate(m *SourceMutation) {
	m.SetName(i.Name)
}

// SetInput applies the change-set in the CreateSourceInput on the SourceCreate builder.
func (c *SourceCreate) SetInput(i CreateSourceInput) *SourceCreate {
	i.Mutate(c.Mutation())
	return c
}

// UpdateSourceInput represents a mutation input for updating sources.
type UpdateSourceInput struct {
	Name *string
}

// Mutate applies the UpdateSourceInput on the SourceMutation builder.
func (i *UpdateSourceInput) Mutate(m *SourceMutation) {
	if v := i.Name; v != nil {
		m.SetName(*v)
	}
}

// SetInput applies the change-set in the UpdateSourceInput on the SourceUpdate builder.
func (c *SourceUpdate) SetInput(i UpdateSourceInput) *SourceUpdate {
	i.Mutate(c.Mutation())
	return c
}

// SetInput applies the change-set in the UpdateSourceInput on the SourceUpdateOne builder.
func (c *SourceUpdateOne) SetInput(i UpdateSourceInput) *SourceUpdateOne {
	i.Mutate(c.Mutation())
	return c
}
