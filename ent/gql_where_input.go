// Code generated by ent, DO NOT EDIT.

package ent

import (
	"errors"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/nibbleshift/grasp/ent/document"
	"gitlab.com/nibbleshift/grasp/ent/predicate"
	"gitlab.com/nibbleshift/grasp/ent/source"
)

// DocumentWhereInput represents a where input for filtering Document queries.
type DocumentWhereInput struct {
	Predicates []predicate.Document  `json:"-"`
	Not        *DocumentWhereInput   `json:"not,omitempty"`
	Or         []*DocumentWhereInput `json:"or,omitempty"`
	And        []*DocumentWhereInput `json:"and,omitempty"`

	// "id" field predicates.
	ID      *uuid.UUID  `json:"id,omitempty"`
	IDNEQ   *uuid.UUID  `json:"idNEQ,omitempty"`
	IDIn    []uuid.UUID `json:"idIn,omitempty"`
	IDNotIn []uuid.UUID `json:"idNotIn,omitempty"`
	IDGT    *uuid.UUID  `json:"idGT,omitempty"`
	IDGTE   *uuid.UUID  `json:"idGTE,omitempty"`
	IDLT    *uuid.UUID  `json:"idLT,omitempty"`
	IDLTE   *uuid.UUID  `json:"idLTE,omitempty"`

	// "name" field predicates.
	Name             *string  `json:"name,omitempty"`
	NameNEQ          *string  `json:"nameNEQ,omitempty"`
	NameIn           []string `json:"nameIn,omitempty"`
	NameNotIn        []string `json:"nameNotIn,omitempty"`
	NameGT           *string  `json:"nameGT,omitempty"`
	NameGTE          *string  `json:"nameGTE,omitempty"`
	NameLT           *string  `json:"nameLT,omitempty"`
	NameLTE          *string  `json:"nameLTE,omitempty"`
	NameContains     *string  `json:"nameContains,omitempty"`
	NameHasPrefix    *string  `json:"nameHasPrefix,omitempty"`
	NameHasSuffix    *string  `json:"nameHasSuffix,omitempty"`
	NameEqualFold    *string  `json:"nameEqualFold,omitempty"`
	NameContainsFold *string  `json:"nameContainsFold,omitempty"`
}

// AddPredicates adds custom predicates to the where input to be used during the filtering phase.
func (i *DocumentWhereInput) AddPredicates(predicates ...predicate.Document) {
	i.Predicates = append(i.Predicates, predicates...)
}

// Filter applies the DocumentWhereInput filter on the DocumentQuery builder.
func (i *DocumentWhereInput) Filter(q *DocumentQuery) (*DocumentQuery, error) {
	if i == nil {
		return q, nil
	}
	p, err := i.P()
	if err != nil {
		if err == ErrEmptyDocumentWhereInput {
			return q, nil
		}
		return nil, err
	}
	return q.Where(p), nil
}

// ErrEmptyDocumentWhereInput is returned in case the DocumentWhereInput is empty.
var ErrEmptyDocumentWhereInput = errors.New("ent: empty predicate DocumentWhereInput")

// P returns a predicate for filtering documents.
// An error is returned if the input is empty or invalid.
func (i *DocumentWhereInput) P() (predicate.Document, error) {
	var predicates []predicate.Document
	if i.Not != nil {
		p, err := i.Not.P()
		if err != nil {
			return nil, fmt.Errorf("%w: field 'not'", err)
		}
		predicates = append(predicates, document.Not(p))
	}
	switch n := len(i.Or); {
	case n == 1:
		p, err := i.Or[0].P()
		if err != nil {
			return nil, fmt.Errorf("%w: field 'or'", err)
		}
		predicates = append(predicates, p)
	case n > 1:
		or := make([]predicate.Document, 0, n)
		for _, w := range i.Or {
			p, err := w.P()
			if err != nil {
				return nil, fmt.Errorf("%w: field 'or'", err)
			}
			or = append(or, p)
		}
		predicates = append(predicates, document.Or(or...))
	}
	switch n := len(i.And); {
	case n == 1:
		p, err := i.And[0].P()
		if err != nil {
			return nil, fmt.Errorf("%w: field 'and'", err)
		}
		predicates = append(predicates, p)
	case n > 1:
		and := make([]predicate.Document, 0, n)
		for _, w := range i.And {
			p, err := w.P()
			if err != nil {
				return nil, fmt.Errorf("%w: field 'and'", err)
			}
			and = append(and, p)
		}
		predicates = append(predicates, document.And(and...))
	}
	predicates = append(predicates, i.Predicates...)
	if i.ID != nil {
		predicates = append(predicates, document.IDEQ(*i.ID))
	}
	if i.IDNEQ != nil {
		predicates = append(predicates, document.IDNEQ(*i.IDNEQ))
	}
	if len(i.IDIn) > 0 {
		predicates = append(predicates, document.IDIn(i.IDIn...))
	}
	if len(i.IDNotIn) > 0 {
		predicates = append(predicates, document.IDNotIn(i.IDNotIn...))
	}
	if i.IDGT != nil {
		predicates = append(predicates, document.IDGT(*i.IDGT))
	}
	if i.IDGTE != nil {
		predicates = append(predicates, document.IDGTE(*i.IDGTE))
	}
	if i.IDLT != nil {
		predicates = append(predicates, document.IDLT(*i.IDLT))
	}
	if i.IDLTE != nil {
		predicates = append(predicates, document.IDLTE(*i.IDLTE))
	}
	if i.Name != nil {
		predicates = append(predicates, document.NameEQ(*i.Name))
	}
	if i.NameNEQ != nil {
		predicates = append(predicates, document.NameNEQ(*i.NameNEQ))
	}
	if len(i.NameIn) > 0 {
		predicates = append(predicates, document.NameIn(i.NameIn...))
	}
	if len(i.NameNotIn) > 0 {
		predicates = append(predicates, document.NameNotIn(i.NameNotIn...))
	}
	if i.NameGT != nil {
		predicates = append(predicates, document.NameGT(*i.NameGT))
	}
	if i.NameGTE != nil {
		predicates = append(predicates, document.NameGTE(*i.NameGTE))
	}
	if i.NameLT != nil {
		predicates = append(predicates, document.NameLT(*i.NameLT))
	}
	if i.NameLTE != nil {
		predicates = append(predicates, document.NameLTE(*i.NameLTE))
	}
	if i.NameContains != nil {
		predicates = append(predicates, document.NameContains(*i.NameContains))
	}
	if i.NameHasPrefix != nil {
		predicates = append(predicates, document.NameHasPrefix(*i.NameHasPrefix))
	}
	if i.NameHasSuffix != nil {
		predicates = append(predicates, document.NameHasSuffix(*i.NameHasSuffix))
	}
	if i.NameEqualFold != nil {
		predicates = append(predicates, document.NameEqualFold(*i.NameEqualFold))
	}
	if i.NameContainsFold != nil {
		predicates = append(predicates, document.NameContainsFold(*i.NameContainsFold))
	}

	switch len(predicates) {
	case 0:
		return nil, ErrEmptyDocumentWhereInput
	case 1:
		return predicates[0], nil
	default:
		return document.And(predicates...), nil
	}
}

// SourceWhereInput represents a where input for filtering Source queries.
type SourceWhereInput struct {
	Predicates []predicate.Source  `json:"-"`
	Not        *SourceWhereInput   `json:"not,omitempty"`
	Or         []*SourceWhereInput `json:"or,omitempty"`
	And        []*SourceWhereInput `json:"and,omitempty"`

	// "id" field predicates.
	ID      *uuid.UUID  `json:"id,omitempty"`
	IDNEQ   *uuid.UUID  `json:"idNEQ,omitempty"`
	IDIn    []uuid.UUID `json:"idIn,omitempty"`
	IDNotIn []uuid.UUID `json:"idNotIn,omitempty"`
	IDGT    *uuid.UUID  `json:"idGT,omitempty"`
	IDGTE   *uuid.UUID  `json:"idGTE,omitempty"`
	IDLT    *uuid.UUID  `json:"idLT,omitempty"`
	IDLTE   *uuid.UUID  `json:"idLTE,omitempty"`

	// "name" field predicates.
	Name             *string  `json:"name,omitempty"`
	NameNEQ          *string  `json:"nameNEQ,omitempty"`
	NameIn           []string `json:"nameIn,omitempty"`
	NameNotIn        []string `json:"nameNotIn,omitempty"`
	NameGT           *string  `json:"nameGT,omitempty"`
	NameGTE          *string  `json:"nameGTE,omitempty"`
	NameLT           *string  `json:"nameLT,omitempty"`
	NameLTE          *string  `json:"nameLTE,omitempty"`
	NameContains     *string  `json:"nameContains,omitempty"`
	NameHasPrefix    *string  `json:"nameHasPrefix,omitempty"`
	NameHasSuffix    *string  `json:"nameHasSuffix,omitempty"`
	NameEqualFold    *string  `json:"nameEqualFold,omitempty"`
	NameContainsFold *string  `json:"nameContainsFold,omitempty"`
}

// AddPredicates adds custom predicates to the where input to be used during the filtering phase.
func (i *SourceWhereInput) AddPredicates(predicates ...predicate.Source) {
	i.Predicates = append(i.Predicates, predicates...)
}

// Filter applies the SourceWhereInput filter on the SourceQuery builder.
func (i *SourceWhereInput) Filter(q *SourceQuery) (*SourceQuery, error) {
	if i == nil {
		return q, nil
	}
	p, err := i.P()
	if err != nil {
		if err == ErrEmptySourceWhereInput {
			return q, nil
		}
		return nil, err
	}
	return q.Where(p), nil
}

// ErrEmptySourceWhereInput is returned in case the SourceWhereInput is empty.
var ErrEmptySourceWhereInput = errors.New("ent: empty predicate SourceWhereInput")

// P returns a predicate for filtering sources.
// An error is returned if the input is empty or invalid.
func (i *SourceWhereInput) P() (predicate.Source, error) {
	var predicates []predicate.Source
	if i.Not != nil {
		p, err := i.Not.P()
		if err != nil {
			return nil, fmt.Errorf("%w: field 'not'", err)
		}
		predicates = append(predicates, source.Not(p))
	}
	switch n := len(i.Or); {
	case n == 1:
		p, err := i.Or[0].P()
		if err != nil {
			return nil, fmt.Errorf("%w: field 'or'", err)
		}
		predicates = append(predicates, p)
	case n > 1:
		or := make([]predicate.Source, 0, n)
		for _, w := range i.Or {
			p, err := w.P()
			if err != nil {
				return nil, fmt.Errorf("%w: field 'or'", err)
			}
			or = append(or, p)
		}
		predicates = append(predicates, source.Or(or...))
	}
	switch n := len(i.And); {
	case n == 1:
		p, err := i.And[0].P()
		if err != nil {
			return nil, fmt.Errorf("%w: field 'and'", err)
		}
		predicates = append(predicates, p)
	case n > 1:
		and := make([]predicate.Source, 0, n)
		for _, w := range i.And {
			p, err := w.P()
			if err != nil {
				return nil, fmt.Errorf("%w: field 'and'", err)
			}
			and = append(and, p)
		}
		predicates = append(predicates, source.And(and...))
	}
	predicates = append(predicates, i.Predicates...)
	if i.ID != nil {
		predicates = append(predicates, source.IDEQ(*i.ID))
	}
	if i.IDNEQ != nil {
		predicates = append(predicates, source.IDNEQ(*i.IDNEQ))
	}
	if len(i.IDIn) > 0 {
		predicates = append(predicates, source.IDIn(i.IDIn...))
	}
	if len(i.IDNotIn) > 0 {
		predicates = append(predicates, source.IDNotIn(i.IDNotIn...))
	}
	if i.IDGT != nil {
		predicates = append(predicates, source.IDGT(*i.IDGT))
	}
	if i.IDGTE != nil {
		predicates = append(predicates, source.IDGTE(*i.IDGTE))
	}
	if i.IDLT != nil {
		predicates = append(predicates, source.IDLT(*i.IDLT))
	}
	if i.IDLTE != nil {
		predicates = append(predicates, source.IDLTE(*i.IDLTE))
	}
	if i.Name != nil {
		predicates = append(predicates, source.NameEQ(*i.Name))
	}
	if i.NameNEQ != nil {
		predicates = append(predicates, source.NameNEQ(*i.NameNEQ))
	}
	if len(i.NameIn) > 0 {
		predicates = append(predicates, source.NameIn(i.NameIn...))
	}
	if len(i.NameNotIn) > 0 {
		predicates = append(predicates, source.NameNotIn(i.NameNotIn...))
	}
	if i.NameGT != nil {
		predicates = append(predicates, source.NameGT(*i.NameGT))
	}
	if i.NameGTE != nil {
		predicates = append(predicates, source.NameGTE(*i.NameGTE))
	}
	if i.NameLT != nil {
		predicates = append(predicates, source.NameLT(*i.NameLT))
	}
	if i.NameLTE != nil {
		predicates = append(predicates, source.NameLTE(*i.NameLTE))
	}
	if i.NameContains != nil {
		predicates = append(predicates, source.NameContains(*i.NameContains))
	}
	if i.NameHasPrefix != nil {
		predicates = append(predicates, source.NameHasPrefix(*i.NameHasPrefix))
	}
	if i.NameHasSuffix != nil {
		predicates = append(predicates, source.NameHasSuffix(*i.NameHasSuffix))
	}
	if i.NameEqualFold != nil {
		predicates = append(predicates, source.NameEqualFold(*i.NameEqualFold))
	}
	if i.NameContainsFold != nil {
		predicates = append(predicates, source.NameContainsFold(*i.NameContainsFold))
	}

	switch len(predicates) {
	case 0:
		return nil, ErrEmptySourceWhereInput
	case 1:
		return predicates[0], nil
	default:
		return source.And(predicates...), nil
	}
}
