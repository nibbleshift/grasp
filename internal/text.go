package main

import (
	"fmt"
	"regexp"

	//"sort"
	"strings"

	textrank "github.com/DavidBelicza/TextRank"
	rake "github.com/afjoseph/RAKE.Go"
	"github.com/jdkato/prose/v2"
)

func Sanitize_string(str string) string {
	reg, err := regexp.Compile("[^a-zA-Z0-9\r\n]+")
	if err != nil {
		fmt.Println(err)
	}
	str = reg.ReplaceAllString(str, " ")

	return str
}

func AssembleText(pageList []Page) string {
	var text string

	for _, page := range pageList {
		text += page.text
	}
	return text
}

func unique(strSlice []string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range strSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func sanitize_array(entries []string) []string {
	//sort.Strings(entries)
	for i, e := range entries {
		entries[i] = strings.ToLower(e)
	}

	return unique(entries)

}

/* returns map with gpe and person arrays*/
func GetEntities(text string) map[string][]string {
	entities := make(map[string][]string)

	doc, err := prose.NewDocument(text)

	if err != nil {
		fmt.Println(err)
	}

	for _, ent := range doc.Entities() {
		entities[ent.Label] = append(entities[ent.Label], ent.Text)
	}

	for k, v := range entities {
		entities[k] = sanitize_array(v)
	}

	return entities
}

func TextRank(text string) ([]string, []*pb.Phrase) {
	var phrases []string
	var top_phrases []*pb.Phrase

	tr := textrank.NewTextRank()
	rule := textrank.NewDefaultRule()
	language := textrank.NewDefaultLanguage()
	algorithmDef := textrank.NewDefaultAlgorithm()

	tr.Populate(text, language, rule)
	tr.Ranking(algorithmDef)

	rankedPhrases := textrank.FindPhrases(tr)

	for _, phrase := range rankedPhrases {
		token := fmt.Sprintf("%s %s",
			phrase.Left,
			phrase.Right)
		phrases = append(phrases, token)
		top_phrases = append(top_phrases, &pb.Phrase{Content: token, Weight: phrase.Weight, Qty: int32(phrase.Qty)})
	}
	return phrases, top_phrases
}

func GetKeywords(text string) []*pb.Keyword {
	var keywords []*pb.Keyword

	text = `The growing doubt of human autonomy and reason has created a state of moral confusion where man is left without the guidance of either revelation or reason. The result is the acceptance of a relativistic position which proposes that value judgements and ethical norms are exclusively matters of arbitrary preference and that no objectively valid statement can be made in this realm... But since man cannot live without values and norms, this relativism makes him an easy prey for irrational value systems.`

	candidates := rake.RunRake(text)

	for _, candidate := range candidates {
		fmt.Printf("%s --> %f\n", candidate.Key, candidate.Value)
		keywords = append(keywords, &pb.Keyword{Value: candidate.Key, Score: candidate.Value})
	}

	fmt.Printf("\nsize: %d\n", len(candidates))
	return keywords
}

func ProcessText(text string) *pb.Text {
	pbText := &pb.Text{}

	entities := GetEntities(text)

	phrases, top_phrases := TextRank(text)

	keywords := GetKeywords(text)

	pbText.Raw = text
	pbText.EntityGpe = entities["GPE"]
	pbText.EntityPerson = entities["PERSON"]
	pbText.Phrases = phrases
	pbText.TopPhrases = top_phrases
	pbText.Keywords = keywords

	return pbText
}
