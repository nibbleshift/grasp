package main

import
(
	"fmt"
	"path"
	"runtime"
	"os"
	"flag"
	"github.com/fsnotify/fsnotify"
	imagick "gopkg.in/gographics/imagick.v2/imagick"
)

var image_jobs = make(chan string, 100)

func generate_image_name(outpath string, sha1 string, page int) string {
	path := create_image_path(outpath, sha1)
	filename := fmt.Sprintf("%s/%s_%d.jpg", path, sha1, page)
	return filename
}

func create_image_path(outpath string, sha1 string) string {
	path := fmt.Sprintf("%s/%s", outpath, sha1[0:4])
	os.MkdirAll(path, os.ModePerm)
	return path
}


func pdf_to_image(mw *imagick.MagickWand, infile string, outpath string) {
	f, err := os.Open(infile)
	defer f.Close()

	if err != nil {
		fmt.Printf("Open failed: %s\n", err)
		return
	}

	sha1, err := hash_file_sha1(f)

	if err != nil {
		fmt.Printf("hash failed: %s\n", err)
		return
	}

	err = mw.ReadImage(infile)

	if err != nil {
		fmt.Printf("ReadImage: %s\n", err)
		return
	}

	n_pages := int(mw.GetNumberImages())

	//mw.SetCompressionQuality(90)
	mw.SetFormat("jpg")
	mw.SetOption("quality", "82")
	mw.SetOption("background", "white")
	mw.SetOption("alpha", "off")

	for i := 0; i < n_pages; i++ {
		mw.SetIteratorIndex(i)

		page_filename := generate_image_name(outpath, sha1, i+1)

		fmt.Printf("Write image: %s\n", page_filename)
		mw.StripImage()
		mw.WriteImage(page_filename)
	}
}

func image_worker(id int, jobs <-chan string) {
        mw := imagick.NewMagickWand()
        defer mw.Destroy()

	outpath := ""
	for j := range jobs {
		pdf_to_image(mw, j, outpath) 
	}
}

func main() {
	var n_cores int
	daemonPtr := flag.Bool("daemon", false, "Enable daemon mode")
	inPtr := flag.String("in", "", "Input PDF filename")
	outdirPtr := flag.String("out", "", "Output directory")
	flag.Parse()

	imagick.Initialize()
	defer imagick.Terminate()

	if *daemonPtr {
		n_cores =  runtime.NumCPU() 
		fmt.Printf("Starting %d workers\n", n_cores)

		for w := 1; w <= n_cores; w++ {
			go image_worker(w, image_jobs)
		}

		watcher, err := fsnotify.NewWatcher()
		if err != nil {
			fmt.Printf("ERROR", err)
		}
		defer watcher.Close()

		done := make(chan bool)

		go func() {
			for {
				select {
				case event := <-watcher.Events:
					if event.Op & fsnotify.Create == fsnotify.Create {
						newPath := fmt.Sprintf("pdf/pending/%s", path.Base(event.Name))
						os.Rename(event.Name, newPath)
						image_jobs <- newPath
						fmt.Printf("Added %s to queue.\n", newPath)
					}

				case err := <-watcher.Errors:
					fmt.Printf("ERROR", err)
				}
			}
		}()

		if err := watcher.Add("pdf/input"); err != nil {
			fmt.Printf("ERROR", err)
		}

		<-done

		fmt.Println("Flux is done...Exiting...")
	} else {
		mw := imagick.NewMagickWand()
		defer mw.Destroy()
		pdf_to_image(mw, *inPtr, *outdirPtr)
	}
}
