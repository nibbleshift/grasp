package main

import (
	"fmt"
	"os"

	"github.com/otiai10/gosseract"
	"github.com/unidoc/unipdf/v3/core"
	"github.com/unidoc/unipdf/v3/extractor"
	pdf "github.com/unidoc/unipdf/v3/model"
	imagick "gopkg.in/gographics/imagick.v2/imagick"
)

func pdf_extract_metadata(uri string) map[string]string {
	m := map[string]string{}

	f, err := os.Open(uri)
	if err != nil {
		return m
	}

	defer f.Close()

	pdfReader, err := pdf.NewPdfReader(f)
	if err != nil {
		return m
	}

	/*numPages, err := pdfReader.GetNumPages()
	if err != nil {
		return nil
	}*/

	trailerDict, err := pdfReader.GetTrailer()
	if err != nil {
		return m
	}

	if trailerDict == nil {
		fmt.Printf("No trailer dictionary -> No DID dictionary\n")
		return m
	}

	var infoDict *core.PdfObjectDictionary

	infoObj := trailerDict.Get("Info")
	switch t := infoObj.(type) {
	case *core.PdfObjectReference:
		infoRef := t
		infoObj, err = pdfReader.GetIndirectObjectByNumber(int(infoRef.ObjectNumber))
		infoObj = core.TraceToDirectObject(infoObj)
		if err != nil {
			return m
		}
		infoDict, _ = infoObj.(*core.PdfObjectDictionary)
	case *core.PdfObjectDictionary:
		infoDict = t
	}

	if infoDict == nil {
		fmt.Printf("DID dictionary not present\n")
		return m
	}

	for _, key := range infoDict.Keys() {
		value, ok := infoDict.Get(key).(*core.PdfObjectString)
		if ok {
			m[string(key)] = value.String()
		}
	}
	return m
}

func pdf_extract_text(uri string) ([]Page, error) {
	var pages []Page

	f, err := os.Open(uri)
	if err != nil {
		return nil, err
	}

	defer f.Close()

	pdfReader, err := pdf.NewPdfReader(f)
	if err != nil {
		return nil, err
	}

	numPages, err := pdfReader.GetNumPages()
	if err != nil {
		return nil, err
	}

	for i := 0; i < numPages; i++ {
		pageNum := i + 1

		page, err := pdfReader.GetPage(pageNum)
		if err != nil {
			return nil, err
		}

		ex, err := extractor.New(page)
		if err != nil {
			return nil, err
		}

		text, err := ex.ExtractText()
		if err != nil {
			return nil, err
		}

		p := Page{
			seq:  i,
			text: text}

		pages = append(pages, p)
	}

	return pages, err
}

func (e *Extract) pdf_ocr_text(uri string) ([]Page, error) {
	var pages []Page
	var imageList []string
	var err error
	mw := imagick.NewMagickWand()
	defer mw.Destroy()

	ocr_client := gosseract.NewClient()
	defer ocr_client.Close()

	err = mw.ReadImage(e.uri)

	if err != nil {
		fmt.Printf("ReadImage: %s\n", err)
	}

	n_pages := mw.GetNumberImages()

	fmt.Printf("NumPages: %d\n", n_pages)
	e.n_pages = int(n_pages)

	err = mw.SetResolution(300, 300)

	if err != nil {
		fmt.Printf("SetResolution: %s\n", err)
	}

	if err = mw.SetImageAlphaChannel(imagick.ALPHA_CHANNEL_FLATTEN); err != nil {
		fmt.Printf("SetImageAlpha: %s\n", err)
	}

	if err := mw.SetCompressionQuality(100); err != nil {
		fmt.Printf("SetCompression: %s\n", err)
	}

	err = mw.SetFormat("tiff")

	if err != nil {
		fmt.Printf("SetImageFormat: %s\n", err)
	}

	err = mw.SetOption("density", "300")

	if err != nil {
		fmt.Printf("SetOptionDensity: %s\n", err)
	}

	err = mw.SetOption("depth", "8")

	if err != nil {
		fmt.Printf("SetOptionDepth: %s\n", err)
	}

	err = mw.SetOption("background", "white")

	if err != nil {
		fmt.Printf("SetBackground: %s\n", err)
	}

	err = mw.SetOption("alpha", "off")

	if err != nil {
		fmt.Printf("SetAlpha: %s\n", err)
	}

	/* convert to images */
	for i := 0; i < e.n_pages; i++ {
		page_filename := fmt.Sprintf("%s/page_%d.tiff", e.tmpdir, i)

		/*
			err = client.SetImage(page_filename)
			if err != nil {
				fmt.Printf("SetImage: %s\n", err)
			}*/

		bIterSet := mw.SetIteratorIndex(i)

		if bIterSet == false {
			fmt.Printf("SetIteratorIndex: SetIteratorIndex(%d) failed", i)
		}

		err = mw.WriteImage(page_filename)

		if err != nil {
			fmt.Printf("WriteImage: %s\n", err)
		} else {
			fmt.Printf("Writing Image: %s\n", page_filename)
		}

		imageList = append(imageList, page_filename)
	}

	/* ocr text */
	for i := 0; i < e.n_pages; i++ {
		ocr_client.SetImage(imageList[i])

		if err != nil {
			fmt.Printf("OCR SetImage: %s\n", err)
		}

		text, err := ocr_client.Text()

		if err != nil {
			fmt.Printf("OCR Error: %s\n", err)
		}

		page := Page{
			seq:  i,
			text: text}

		pages = append(pages, page)
	}
	return pages, nil
}
