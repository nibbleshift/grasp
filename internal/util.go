package main

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/davecgh/go-spew/spew"
	"github.com/h2non/filetype"
	"github.com/rwcarlsen/goexif/exif"
	"github.com/rwcarlsen/goexif/tiff"
)

type Scanner func(string) bool

type walkFunc func(exif.FieldName, *tiff.Tag) error

func (f walkFunc) Walk(name exif.FieldName, tag *tiff.Tag) error {
	return f(name, tag)
}

type walker struct {
	picName string
}

func PrettyPrint(v interface{}) (err error) {
	spew.Dump(v)
	return
}

func Visit(files *[]string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Printf("blah: %s", err)
		}
		if info.IsDir() == false {
			*files = append(*files, path)
		}
		return nil
	}
}

func LoadIndexScanner(uri string, m *map[string]bool) bool {
	rec := LoadRecord(uri)
	if rec == nil {
		return false
	}
	(*m)[rec.Sha1] = true
	return true
}

func Scanindex(uri string, fn Scanner) {
	var files []string
	fmt.Printf("Loading index from %s\n", uri)
	err := filepath.Walk(uri, Visit(&files))
	if err != nil {
		panic(err)
	}

	for _, file := range files {
		fn(file)
	}
}

func LoadIndex(uri string) map[string]bool {
	var files []string
	fmt.Printf("Loading index Map from %s\n", uri)
	err := filepath.Walk(uri, Visit(&files))
	if err != nil {
		panic(err)
	}

	m := make(map[string]bool)

	for _, file := range files {
		rec := LoadRecord(file)

		if rec == nil {
			continue
		}

		m[rec.Sha1] = true
	}
	fmt.Printf("Loaded map index with %d entries\n", len(m))

	return m
}

func hash_file_sha1(file *os.File) (string, error) {
	var returnSHA1String string

	hash := sha1.New()
	if _, err := io.Copy(hash, file); err != nil {
		return returnSHA1String, err
	}
	hashInBytes := hash.Sum(nil)[:20]
	returnSHA1String = hex.EncodeToString(hashInBytes)
	return returnSHA1String, nil
}

func get_file_type(uri string) string {
	buf, _ := ioutil.ReadFile(uri)

	kind, err := filetype.Match(buf)

	if err != nil {
		return ""
	}

	if kind == filetype.Unknown {
		fmt.Println("Unknown file type")
		return ""
	}
	return kind.MIME.Value
}
